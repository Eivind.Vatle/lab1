package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");



    public void run() {
        // TODO: Implement Rock Paper Scissors
        //Use a while(true) loop and only break the loop if the user wants to quit
        while(true) {


            //Get the user's move through user input
            System.out.println("Let's play round "+ roundCounter);
            String HumanMove = readInput("Your choice (Rock/Paper/Scissors)? ");
            roundCounter += 1;




            //Checking if the user wrote an acceptable move
            if(!HumanMove.equals("rock") && !HumanMove.equals("paper") && !HumanMove.equals("scissors")) {

                System.out.println("I do not understand " + HumanMove + " Could you try again?");
                continue;


            } else {


                //Deciding the computers move with a random number form 1-3
                int RandomNumber = (int)(Math.random()*3);
                String ComputerMove = "";
                if(RandomNumber == 0) {
                    ComputerMove = "rock";
                } else if(RandomNumber == 1) {
                    ComputerMove = "paper";
                } else {
                    ComputerMove = "scissors";

                }


                //Printing the results
                if(HumanMove.equals(ComputerMove)) {
                    System.out.println("Human chose "+ HumanMove +" computer chose " + ComputerMove + " . It's a tie! ");


                } else if((HumanMove.equals("rock") && ComputerMove.equals("scissors")) || (HumanMove.equals("scissors") && ComputerMove.equals("paper")) || (HumanMove.equals("paper") && ComputerMove.equals("rock"))) {
                    System.out.println("Human chose " + HumanMove + " computer chose " + ComputerMove + " Human wins!");
                    humanScore += 1;


                } else {
                    System.out.println("Human chose " + HumanMove + " computer chose " + ComputerMove + " Computer wins!");
                    computerScore += 1;

                }
                //printing the score of the game
                System.out.println("Score: human "+humanScore+" computer score: "+computerScore);


                //Asking whether the player wants to keep playing
                String NewGameQuestion = readInput("Do you wish to continue playing? (y/n)?");
                if(NewGameQuestion.equals("n")) {
                    break;
                }

            }

        }



        //Printing ending message
        System.out.println("Bye bye :)");


    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next().toLowerCase();
        return userInput;
    }

}
